---
layout: post
title:  "Week Thirteen"
date:   2018-12-02 12:00:00 -0600
categories: blog
---
![Ivan]({{"/assets/ivan.png" | absolute_url}}){:style="margin: 0 auto; display: block;"}


<h3>What did I do this past week?</h3>
This past week we continued the talks on refactoring and my group worked on our final presentation. We have everything planned out and we know what we are going to say. However, since our schedules don't line up, we have not had a chance to practice presenting together.

<h3>What's in my way?</h3>
There is nothing in my way as of now. I feel pretty confident about the upcoming midterm mostly because I have not had much trouble with the HackerRank exercises that we have been doing in class. Also, after we present there will be no projects that I have to worry about, so I am just coasting. However, the only thing I am uncertain about is what to put on my cheat sheet.

<h3>What will I do next week?</h3>
Next week I will present on Monday and then just sit back, relax, and watch the rest of the presentations. I also plan on reviewing material for the second midterm.

<h3>What did I think of the talk by Google?</h3>
Sadly I was not able to go to the talk because I have ultimate frisbee practice every Monday, Wednesday, and Friday. I wish we had talks on Thursday nights instead.

<h3>What was my experience of the refactoring topics?</h3>
I really enjoyed talking about refactoring because it is rare that we discuss how to write clean and elegant code. Most of what we learn in CS classes is how to tackle specific problems related to networking, or graphics, or even AI, but we never learn the best way to go about implementing our solutions.

<h3>What's my pick-of-the-week or tip-of-the-week?</h3>
If you have ever needed to use Git, you might have noticed that it is really easy to use when you do everything correctly, but if you have had the misfortune of making a typo in your commit message, you know how unintuitive git becomes. Undoing things in Git is pretty tricky unless you know what you are doing, and it can also have devastating consequences. However, there is [this helpful guide](https://blog.github.com/2015-06-08-how-to-undo-almost-anything-with-git/) to help you undo (almost) anything with Git.

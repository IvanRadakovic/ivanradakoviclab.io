---
layout: post
title:  "Final Entry"
date:   2018-12-11 12:00:00 -0600
categories: blog
---
![Ivan]({{"/assets/ivan.png" | absolute_url}}){:style="margin: 0 auto; display: block;"}


<h3>What did you like the least about the class?</h3>
My least favorite thing about the class was how the quizzes were structured. At times, they were pretty vague, so it was difficult to determine what they were asking of you. It usually took a few questions to understand what concept they were testing, and by then it was too late because you could not go back to fix your answers.

<h3>What did you like the most about the class?</h3>
By far, the best thing about this class was the lectures because Downing is a very knowledgeable and engaging lecturer. He explains very specific nuances of Python by walking students through a problem. By asking us the right questions, he enabled us to come to our own conclusions about the inner workings of Python. Another great thing about the class was how Downing made a personal connection with all the students by calling on us and working through the problems with us instead of lecturing to the class as a whole. It makes the class feel so much small and more personal.

<h3>What's the most significant thing you learned?</h3>
The most significant thing I learned in this class was the new outlook on getters and setters. In all previous CS classes, we were taught how you should always use getters and setters. All they ended up being used for was to give information to another object to it could do some work. However, a much more elegant approach is to have the object that already has the information do the work, and return the result. For this reason, traditional getters and setters should be avoided most of the time.

<h3>How many hours a week did you spend coding for this class?</h3>
I usually spent around 10 hours every other week coding. The projects for this class are not very challenging if you start early, and they are made easier if your group member have experience with the tools.

<h3>How many hours a week did you spend reading for this class?</h3>
The reading workload for this class was very light. I found myself spending less than an hour per week reading.

<h3>How many lines of code do you think you wrote?</h3>
Over the course of the entire class, I ended up writing 1500-2000 lines of code. Although I did not write a lot of code, I put in quite a bit of thought into the code that I did write.

<h3>What required tool did you not know and now find very useful?</h3>
Before taking this class, I had no idea what Docker was, but now I think it's invaluable. Docker lets you create containers which are basically dev environments so that everyone working on a project has identical versions of the software. This is great for avoiding the dreaded "well it works on my machine" response from people.

<h3>What's the most useful web dev tool that your group used that was not required?</h3>
We used Material UI as the frontend theme and it made styling and unifying different parts of our site easy.

<h3>If you could change one thing about the course, what would it be?</h3>
I wish we were given more details of what is expected for the projects. The rubric on Canvas is good, but I would prefer it to have more details. In addition, it would be nice that same rubric was posted on the assignment pages.

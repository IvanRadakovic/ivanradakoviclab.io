---
layout: post
title:  "Week Twelve"
date:   2018-11-18 12:00:00 -0600
categories: blog
---
![Ivan]({{"/assets/ivan.png" | absolute_url}}){:style="margin: 0 auto; display: block;"}


<h3>What did I do this past week?</h3>
This week, my team and I finished Project 4 which turned out to be a lot less work than the previous projects. In class, we continued to talk about refactoring and all the methods that we can use to make the code easier to read and maintain. One thing that was surprising to me was that we should avoid using getters and instead have the class with the data do the work. In all my time in computer science, I was taught to always use getters and setters.

<h3>What's in my way?</h3>
There is currently nothing in my way. By this point in the semester, I have figured out how to manage my time with all my classes, projects, band, and frisbee. I have been doing fairly well on the quizzes and projects. Also, I do not have much trouble keeping up in class because if there is something I do not understand, I review on my own after class.

<h3>What will I do next week?</h3>
Next week, I plan on going back home to Houston for Thanksgiving break. I cannot wait to see my family and friends from high school. I will most likely relax because this will be the first week I will have free since spring break. That being said, school never quite stops because I have my final graphics project that is due the week after I get back that I might want to start working on.

<h3>What was my experience of Project #4?</h3>
Adding searching and sorting was very easy to do because we did all the work on the backend. This allowed us to spend more time cleaning up and polishing the frontend. Overall, I enjoyed this project because it allowed up to add meaningful features to our site without a lot of grunt work.

<h3>What's my pick-of-the-week or tip-of-the-week?</h3>
My tip-of-the-week is the [Teletype package](https://teletype.atom.io/) for the [Atom editor](https://atom.io/). This package enables you to share your workspace in real-time similar to Google Docs. This is really useful for pair programming when the people collaborating cannot be in the same room. One of the best features is that the central server is only used to connect people together, and the collaborators share code directly peer-to-peer. Teletype uses WebRTC to encrypt everything so you do not have to worry about anyone stealing your code.h thousands of community made packages.

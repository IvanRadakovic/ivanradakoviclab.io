---
layout: post
title:  "The Ethical Software Engineer"
date:   2018-10-30 12:00:00 -0600
categories: blog
---
![Ivan]({{"/assets/ivan.png" | absolute_url}}){:style="margin: 0 auto; display: block;"}


<h3>The Suggestion Box</h3>
If I was presented with this situation, I would definitely look through the activity logs to help the owner determine the identity of the person who threatened to retaliate. This is the only ethical response because turning out defective parts could be detrimental for several reasons. The hypothetical situation describes that we are working for a manufacturing company, and as a result, producing defective parts could result in people getting hurt down the line. Best case scenario, we are making screws for tables, and a defective part would make some regret buying Ikea furniture again in hope that it would last longer than a year. However, we could be responsible for making brake pads for cars or airplane components. If our company produced unreliable parts, and this went unchecked it could result in the loss of lives. Ultimately I would feel responsible if my lack of action led to some disaster. Also, if our company gains a reputation for producing faulty products, it could lead to a loss of business and the company going bankrupt. This would mean that 200+ people would be without a job. Although anonymity was promised, in certain situations it can and should be revoked. If maintaining privacy would result in harm to people, it should not be expected. For example, the government provides rights to privacy and free speech, but if you were to make a threat to public safety or to any public officials, your rights quickly disappear. Even if the employee made the threat as a joke or as an experiment to see if people's responses would stay anonymous, it should be treated as a serious matter. There are other means to find out who sent in the grievance, but they are either too costly or may not be effective enough. For example, if the managers know that only one person is producing defective parts, they could test everything that comes through the factory, but that would cost way too much money and waste company time. Also, the managers could interview everyone to get to the bottom of this, but that would take even more time for a 200+ person company. In addition, there is no guarantee that they will even be able to figure out who the person was. When people's lives are on the line, time is of the essence.

However, if defects would not cause harm to people, then I would be reluctant to expose someone's privacy. Also, if the boss threatened to publicly humiliate or fire the employee I would not endorse this behavior. Due to the fact that the boss promised anonymity, not only is it unethical to break that promise, but it would have large-scale consequences throughout the company. If people find out what happened, they would be less reluctant to bring up issues in the future. They would probably lose any trust they had in the company. Nevertheless, the only thing to justify intruding on people's privacy is if protecting it could result in people getting hurt.




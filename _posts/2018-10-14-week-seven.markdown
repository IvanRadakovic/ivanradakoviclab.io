---
layout: post
title:  "Week Seven"
date:   2018-10-14 12:00:00 -0600
categories: blog
---
![Ivan]({{"/assets/ivan.png" | absolute_url}}){:style="margin: 0 auto; display: block;"}


<h3>What did I do this past week?</h3>
This past week we learned about argument packing and unpacking. Specifically, we discussed all the different meanings =, \*, \*\* both in a function definition and function call.

<h3>What's in my way?</h3>
The only thing that is on my mind right now is the midterm tomorrow. Even though I am fairly confident with Python, I am a little worried about the multiple choice questions. The quizzes always trip me up a little because it takes me a couple questions to realize what the questions are asking. I am hoping that this will not be a problem on the midterm because we will be able to go back and look at questions we already answered.

<h3>What will I do next week?</h3>
Next week, my team and I plan on starting up the IDB project again. We put the breaks on for a week while we prepared for the midterm. Once we get that out of the way we plan on meeting to discuss what needs to be done next and assign tasks. I will refresh my React knowledge before we begin the next phase of the project because it has been a few months since I have used it last.

<h3>What was my experience of learning the basics of Python?</h3>
I really enjoyed learning all the nuances of Python because anyone can quickly learn basic syntax in a week and start writing code. However, it takes a thorough knowledge of a language to apply best practices to make your code efficient and easy to read. It was fascinating to learn all the "trivia" of Python and less known features. Also, by understanding a language at this level, it prevents simple logic errors and bugs. For example, knowing that closures capture the variable itself instead of the value of the variable can save hours of debugging.

<h3>What's my pick-of-the-week or tip-of-the-week?</h3>
My pick-of-the-week is [RegExr](https://regexr.com/). RegExr is an online tool to learn, build, and test regular expressions. You can type a regular expression and see what it will match. This really useful whenever you are trying to parse a website, document, or any large data set.

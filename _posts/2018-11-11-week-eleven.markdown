---
layout: post
title:  "Week Eleven"
date:   2018-11-11 12:00:00 -0600
categories: blog
---
![Ivan]({{"/assets/ivan.png" | absolute_url}}){:style="margin: 0 auto; display: block;"}


<h3>What did I do this past week?</h3>
This past week we continued learning SQL and practiced writing queries in class together. What this made me realize was that when professor Downing lectured about how to construct specific queries it was very straightforward. However, whenever we needed to write them on our own I knew what I wanted to do, but I struggled a little bit with the syntax. Outside of class, my team was working on the next phase of the IDB project. We decided to implement searching and sorting on the back end and use the front end to simply interact with our API.

<h3>What's in my way?</h3>
The only thing I am unsure of in this class right now is the correct SQL syntax. My plan is to practice writing SQL queries to help prepare for the second midterm. Besides that, my team and I are doing well on our project.

<h3>What will I do next week?</h3>
Next week, I plan on wrapping up the IDB project by writing acceptance tests and cleaning up the CSS on our front end. Even though it looks good enough, I want our website to feel really polished.

<h3>What was my experience in learning SQL?</h3>
I really enjoyed finally getting to learn SQL because it is a very useful skill that I had never gotten around to learning. What I found most interesting was that SQL is basically magic and applies the queries in the most logical order every time. Although I have no prior experience writing queries, I found it to be quite easy because it feels like you are writing a sentence in English. We also talked about regular expressions which was exciting because I find them to be really useful and underutilized.

<h3>What's my pick-of-the-week or tip-of-the-week?</h3>
If you ever need to perform any mathematical calculations, then an online calculator is a must. I have come to rely on [Cymath](https://www.cymath.com/). It is really easy to use, and it has a clean interface. On top of that, it shows you how to work out the answer so that you can maybe even learn something in the process.

---
layout: post
title:  "Week Eight"
date:   2018-10-21 12:00:00 -0600
categories: blog
---
![Ivan]({{"/assets/ivan.png" | absolute_url}}){:style="margin: 0 auto; display: block;"}


<h3>What did I do this past week?</h3>
On Monday, we took the first midterm, and it well pretty well for me. On Wednesday, we met with different groups and acted as developers/customers. We discussed the purpose of our site, explained our data sources, gave feedback to the other team, and made requests. At the end of the week, we began talking about the next major topic, relational algebra. We learned about the operations select and project.

<h3>What's in my way?</h3>
Right now, I need to brush up on React. I vaguely remember the syntax and the way components work with each other, but I certainly do not know the most idiomatic way to structure the project. My team and I also just realized that there is still a lot left to do on the project, so we will really need to focus and work hard this week.

<h3>What will I do next week?</h3>
Next week, I plan on meeting with my team to work on the next phase of the project. We have to set up our database and get the api working on the backend. I plan on leading the frontend side of the project because I have the most experience with React. After I building the project, I plan on developing the skeleton of the app so others can begin working on different components.

<h3>What was my experience with Test #1 (the problems, the time, HackerRank)?</h3>
I thought the first test was very straightforward. I finished in less than 30 min and that was after I went back and double checked my work twice. The problems were good because they covered exactly what we learned in class. Also, I had no problems with HackerRank because I was already used to taking HackerRank tests from OOP. I also felt that we had plenty of time for the test.

<h3>What's my pick-of-the-week or tip-of-the-week?</h3>
My pick-of-the-week this week is Git Bash. It's a command-line shell that can be run on Windows to get most of the features of a Unix terminal. Instead of having to install PuTTY to ssh, Git Bash is usually installed whenever you install git.
---
layout: post
title:  "Week Three"
date:   2018-09-16 12:00:00 -0600
categories: blog
---
![Ivan]({{"/assets/ivan.png" | absolute_url}}){:style="margin: 0 auto; display: block;"}


<h3>What did I do this past week?</h3>
This week, we started the week by talking about the optimizations we can make on the Collatz project. We also learned about exceptions and the different data types in Python including list, tuple, set, and dict. This was very helpful to me because I had no exposure to Python prior to this class. Outside of class, I completed most of Collatz without much trouble. It was straightforward because I used the same optimizations from the Collatz project from OOP and converted them to Python.

<h3>What's in my way?</h3>
There is currently nothing in my way. Even though I had never coded in Python before this project, it was very easy to pick up. I had assumed that Python was going to be really simple from what I have heard from others, but I was still surprised at how straight-forward it is.

<h3>What will I do next week?</h3>
Next week, I plan on getting an early start on the project because I am taking two other classes that are very project-heavy and I do not want to fall behind. I will also be wrapping up collatz. The only thing I need to do is the git log, readme, and then submit the project on Canvas. In addition, I plan on reading the textbook and articles for the week to come.

<h3>What is my experience of the in-class exercises?</h3>
I really like the in-class exercises because it gives us a chance to practice we just learned. This way we can see if we really understand the concepts or if we only thought that we did. And if we did not quite grasp the material, that's okay because the exercises are not graded, so we can learn from our mistakes.

<h3>What's my pick-of-the-week or tip-of-the-week?</h3>
My tip-of-the-week is the chrome extension [Grammarly](https://www.grammarly.com/). I use it on a weekly basis to write these blogs. The way it works is that it checks your grammar and spelling as you type in any text field online. This is immensely useful for everything from writing emails to answering questions on Piazza. It works a lot better than the built-in browser spell checker because it also finds grammar mistakes.